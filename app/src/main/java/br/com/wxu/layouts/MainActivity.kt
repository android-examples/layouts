package br.com.wxu.layouts

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    private val buttonTableLayout by lazy { findViewById<Button>(R.id.button_table_layout) }
    private val buttonRelativeLayout by lazy { findViewById<Button>(R.id.button_relative_layout) }
    private val buttonFrameLayout by lazy { findViewById<Button>(R.id.button_frame_layout) }
    private val buttonLinearLayout by lazy { findViewById<Button>(R.id.button_linear_layot) }
    private val buttonGridLayout by lazy { findViewById<Button>(R.id.button_grid_layout) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonTableLayout.setOnClickListener {
            val intent = Intent(this@MainActivity, TableLayoutActivity::class.java)
            startActivity(intent)
        }

        buttonRelativeLayout.setOnClickListener {
            val intent = Intent(this@MainActivity, RelativeLayoutActivity::class.java)
            startActivity(intent)
        }

        buttonFrameLayout.setOnClickListener {
            val intent = Intent(this@MainActivity, FrameLayoutActivity::class.java)
            startActivity(intent)
        }

        buttonLinearLayout.setOnClickListener {
            val intent = Intent(this@MainActivity, LinearLayoutActivity::class.java)
            startActivity(intent)
        }

        buttonGridLayout.setOnClickListener {
            val intent = Intent(this@MainActivity, GridLayoutActivity::class.java)
            startActivity(intent)
        }
    }
}
