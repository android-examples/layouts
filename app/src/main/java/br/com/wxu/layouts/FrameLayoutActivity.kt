package br.com.wxu.layouts

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class FrameLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame_layout)

        supportActionBar?.title = "Frame Layout Example"
    }
}
