package br.com.wxu.layouts

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class RelativeLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_layout)

        supportActionBar?.title = "Relative Layout Example"
    }
}
