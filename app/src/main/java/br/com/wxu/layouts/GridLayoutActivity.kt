package br.com.wxu.layouts

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class GridLayoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_layout)

        supportActionBar?.title = "Grid Layout Example"
    }
}
